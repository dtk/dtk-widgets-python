// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

void qwidgetSetLayout(class QWidget *, class QLayout *);

class dtkwidgets_glue {};

//
// dtkwidgets_glue.h ends here
