# dtkWidgetsPython

Python bindings for dtk-widgets.

## Prerequisites

``` shell
$ brew install llvm
$ brew install python
```

## Install

``` python
$ python -m pip install --index-url=http://download.qt.io/snapshots/ci/pyside/5.11/latest pyside2 --trusted-host download.qt.io
```

## Test

``` python
>>> import sys
>>> from PySide2.QtWidgets import QApplication, QLabel
>>> app = QApplication([])
>>> label = QLabel("Hello World")
>>> label.show()
>>> sys.exit(app.exec_())
```

## Uninstall

``` shell
$ python -m pip uninstall pyside2
Uninstalling PySide2-5.11.1a1.dev1530708810518:
  Would remove:
    /usr/local/bin/pyside2-uic
    /usr/local/lib/python3.7/site-packages/PySide2-5.11.1a1.dev1530708810518-5.11.1.dist-info/*
    /usr/local/lib/python3.7/site-packages/PySide2/*
    /usr/local/lib/python3.7/site-packages/pyside2uic/*
Proceed (y/n)? y
  Successfully uninstalled PySide2-5.11.1a1.dev1530708810518
```

## Build

``` shell
$ git clone http://code.qt.io/cgit/pyside/pyside-setup.git pyside
$ cd pyside
$ git checkout 5.11
$ export LLVM_INSTALL_DIR=/usr/local/opt/llvm
$ python2 setup.py install
$ python3 setup.py install
```

Relevant files for development
- ./pyside3_install/py3.7-qt5.11.1-64bit-release/lib/cmake/Shiboken2-5.11.1/Shiboken2Config.cmake
- ./pyside3_install/py3.7-qt5.11.1-64bit-release/lib/cmake/PySide2-5.11.1/PySide2Config.cmake
- ./build/lib.macosx-10.14-x86_64-3.7/PySide2/shiboken2

## Test

``` python
>>> import sys
>>> from PySide2.QtWidgets import QApplication, QLabel
>>> app = QApplication([])
>>> label = QLabel("Hello World")
>>> label.show()
>>> sys.exit(app.exec_())
```

## Build

``` shell
$ git clone git@gitlab.inria.fr:dtk/dtk-widgets.git
$ git clone git@gitlab.inria.fr:dtk/dtk-widgets-python.git
$ cd dtk-widgets; mkdir build; cd build; cmake ..; make
$ cd dtk-widgets-python; mkdir build; cd build; cmake ..; make
```

## Install

``` shell
$ python -m site
$ cd /usr/local/lib/python2.7/site-packages # OR
$ cd /Users/jwintz/Library/Python/2.7/lib/python/site-packages
$ mkdir dtk
$ cp ~/Development/dtk-widgets-python/build/lib/dtkwidgets.so dtk/
$ touch dtk/__init__.py
$ # insert the following
```

``` python
__all__ = list("dtk" + body for body in "widgets".split(";"))
```

# Test

``` python
>>> import sys
>>> from PySide2.QtWidgets import QApplication, QLabel
>>> from dtk.dtkcore import **
>>> app = QApplication([])
>>> layout = dtkWidgetsLayout()
>>> layout.show()
>>> sys.exit(app.exec_())
```
